﻿using System;
using CloudFlare.API;
using CloudFlare.API.Enums;

namespace Console
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                // RETRIEVE COMPLETE DOMAINS LIST
                System.Console.WriteLine("DOMAINS LIST\n----------------------------------------------");
                var domains = CFProxy.Access.GetDomains();
                foreach (var zone in domains.objs)
                {
                    System.Console.WriteLine("{0} - {1} ({2})", zone.zone_id, zone.zone_name, zone.display_name);
                }
                System.Console.WriteLine("");

                if (domains.count > 0)
                {
                    // RETRIEVE FIRST DOMAIN STATISTICS
                    string domain = domains.objs[0].zone_name;
                    System.Console.WriteLine("STATS FOR {0}\n----------------------------------------------", domain);
                    var stats = CFProxy.Access.GetStats(domain, Intervals.Days30);
                    if (stats.count > 0)
                    {
                        System.Console.WriteLine("PageViews: {0}", stats.objs[0].trafficBreakdown.pageviews.crawler +
                                                                   stats.objs[0].trafficBreakdown.pageviews.regular +
                                                                   stats.objs[0].trafficBreakdown.pageviews.threat);
                        System.Console.WriteLine("Visits: {0}", stats.objs[0].trafficBreakdown.uniques.crawler +
                                                                stats.objs[0].trafficBreakdown.uniques.regular +
                                                                stats.objs[0].trafficBreakdown.uniques.threat);
                    }
                    System.Console.WriteLine("");

                    // RETRIEVE COMPLETE DNS RECORDS LIST FOR FIRST DOMAIN
                    System.Console.WriteLine("DNS RECORDS FOR {0}\n----------------------------------------------", domain);
                    var records = CFProxy.Access.GetRecords(domain);
                    foreach (var dnsObject in records)
                    {
                        System.Console.WriteLine("{0}\t{1}\t -> {2}", dnsObject.type, dnsObject.name, dnsObject.content);
                    }
                }
            }
            catch (ApplicationException ex)
            {
                var error = String.Format("ERROR: {0}", ex.Message);
                if (ex.InnerException != null)
                    error += String.Format("\nInner exception: {0}", ex.InnerException.Message);

                System.Console.WriteLine(error);
            }
            System.Console.ReadLine();
            
            return;
            //var result = Dns.Add("test", "www.stagelearning.com", "estratomedia.org", DnsRecordKind.CNAME);
            //System.Console.WriteLine("Added CNAME test -> www.stagelearning.com TO Domain estratomedia.org");
            //System.Console.ReadLine();

            //result = Dns.Edit(result.rec_id, "test", "www.stagelearning.com", "estratomedia.org", DnsRecordKind.CNAME, ServiceMode.On);
            //System.Console.WriteLine("Edited CNAME test -> www.stagelearning.com TO Enable CloudFlare");
            //System.Console.ReadLine();

            //Dns.Delete(result.rec_id, "estratomedia.org");
            //System.Console.WriteLine("Deleted CNAME test on Domain estratomedia.org");
            //System.Console.ReadLine();

        }
    }
}
