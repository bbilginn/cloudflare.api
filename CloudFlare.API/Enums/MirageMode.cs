﻿namespace CloudFlare.API.Enums
{
    /// <summary>
    /// Status for the IPV6 support mode
    /// </summary>
    /// 9/7/2013 by Sergi
    public enum MirageMode
    {
        Off = 0,
        On = 1
    }
}
