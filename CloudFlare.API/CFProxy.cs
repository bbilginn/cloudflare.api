﻿using System;
using System.Net;
using CloudFlare.API.Data;
using CloudFlare.API.Enums;
using CloudFlare.API.Methods;
using CloudFlare.API.Utils;
using RestSharp;
using Dns = CloudFlare.API.Methods.Dns;

namespace CloudFlare.API
{
    /// <summary>
    /// Proxy class to access the API 
    /// </summary>
    /// 9/5/2013 by Sergi
    public static class CFProxy
    {
        #region " Attributes "

        // Objects to encapsulate API methods
        public static Access Access = new Access();
        public static Dns Dns = new Dns();
        public static Modify Modify = new Modify();

        // Config values
        public static CFConfig Config = new CFConfig();

        // API Access Configuration
        private static readonly string BaseUrl = Config.BaseUrl;
        private static readonly string _apiKey = Config.ApiKey;
        private static readonly string _email = Config.Email;

        #endregion " Attributes "

        #region " Public methods "

        /// <summary>
        /// Executes the request and tries to deserialize the response to T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// 9/5/2013 by Sergi
        public static T Execute<T>(RestRequest request) where T : new()
        {
            var client = new RestClient
                {
                    BaseUrl = BaseUrl,
                    Authenticator = new SimpleAuthenticator(CFParameters.ApiKey, _apiKey, CFParameters.Email, _email)
                };
            var response = client.Execute<T>(request);

            // Checking call success
            HandleRequestError(response);
            HandleResponseError(response.Data as ResponseBase);

            return response.Data;
        }

        #endregion " Public methods "

        #region " Private methods "

        /// <summary>
        /// Handles errors in the request (Network, transport, etc.)
        /// </summary>
        /// <param name="response">The response.</param>
        /// 9/6/2013 by Sergi
        private static void HandleRequestError(IRestResponse response)
        {
            bool success = true;
            string message = "";
            if (response.ResponseStatus != ResponseStatus.Completed)
            {
                message = String.Format("Network error. Response status: {0}. Error: {1}", response.ResponseStatus.ToString(), response.ErrorMessage);
                success = false;

            }
            if (response.StatusCode != HttpStatusCode.OK)
            {
                message = String.Format("Server error. HTTP Status code: {0}. Error: {1}", response.StatusCode.ToString(), response.Content);
                success = false;
            }
            if (response.ErrorException != null)
            {
                message = "Error retrieving response. Check inner details for more info.";
                success = false;
            }
            if (success) return;
            
            var proxyException = new ApplicationException(message, response.ErrorException);
            throw proxyException;
        }

        /// <summary>
        /// Handles an error in the remote service.
        /// </summary>
        /// <param name="response">The response.</param>
        /// 9/6/2013 by Sergi
        private static void HandleResponseError(ResponseBase response)
        {
            if (response.result == ResultKind.Success.GetStringValue()) return;
            
            var proxyException = new ApplicationException(response.msg);
            throw proxyException;
        }

        #endregion " Private methods "
    }
}
