﻿using System;
using System.Collections.Generic;

namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the result messages for Domains related requests
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - zones: {Zones}
    ///             
    /// </summary>
    public class DomainResponse : ResponseBase
    {
        public DomainResult response { get; set; }
    }

    public class ZoneResponse : ResponseBase
    {
        public ZoneResult response { get; set; }
    }

    public class DomainResult
    {
        public Zones zones { get; set; }
    }

    public class ZoneResult
    {
        public string expires_on { get; set; }
        public ZoneRecord zone { get; set; }
    }

    public class ZoneRecord
    {
        public ZoneObject obj  { get; set; }
    }

    public class Zones
    {
        public Int32 count { get; set; }
        public Boolean has_more { get; set; }
        public List<ZoneObject> objs { get; set; }
    }

    /// <summary>
    /// Data related to a domain
    /// </summary>
    public class ZoneObject
    {
        public Int64 zone_id { get; set; }
        public Int64 user_id { get; set; }
        public String zone_name { get; set; }
        public String display_name { get; set; }
        public String zone_status { get; set; }
        public int zone_mode { get; set; }
        public Int64 host_id { get; set; }
        public string zone_type { get; set; }
        public string host_pubname { get; set; }
        public string host_website { get; set; }
        public string vtxt { get; set; }
        public List<string> fqdns { get; set; }
        public string step { get; set; }
        public string zone_status_class { get; set; }
        public string zone_status_desc { get; set; }
        public List<string> ns_vanity_map { get; set; }
        public string orig_registrar { get; set; }
        public string orig_dnshost { get; set; }
        public string orig_ns_names { get; set; }
        public ZoneProps props { get; set; }
        public ConfirmCode confirm_code { get; set; }
        public List<String> allow { get; set; }
    }

    public class ConfirmCode
    {
        public string zone_deactivate { get; set; }
        public string zone_dev_mode1 { get; set; }
    }

    public class ZoneProps
    {
        public int dns_cname { get; set; }
        public int dns_partner { get; set; }
        public int dns_anon_partner { get; set; }
        public int pro { get; set; }
        public int expired_pro { get; set; }
        public int pro_sub { get; set; }
        public int ssl { get; set; }
        public int expired_ssl { get; set; }
        public int expired_rs_pro { get; set; }
        public int reseller_pro { get; set; }
        public int force_interal { get; set; }
        public int ssl_needed { get; set; }
        public int alexa_rank { get; set; }
    }
}
