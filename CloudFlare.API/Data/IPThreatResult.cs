﻿using System;
using System.Collections.Generic;

namespace CloudFlare.API.Data
{
    /// <summary>
    /// Class for mapping the result messages for Purge cache related request
    /// The hierarchy is:
    ///     - request (from ResponseBase)
    ///     - result (from ResponseBase)
    ///     - msg (from ResponseBase)
    ///     - response: the response message
    ///         - zone: {Zone}
    ///             - obj: {ZoneObject}
    ///             
    /// </summary>
    public class IPThreatResponse : ResponseBase
    {
        public Dictionary<string, string> response { get; set; }
    }
}
